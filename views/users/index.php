<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'userId',
            'firstName:ntext',
            'lastName:ntext',
            'email:ntext',
            'personalCode',
            // 'phone',
            // 'active:boolean',
            // 'isDead:boolean',
            // 'lang:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
      <?= Html::a('Populate Users', ['populate'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
